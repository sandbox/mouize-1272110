README.txt
==========

This module was created for a very specific need: to have a graphic rendering
of the results Behat.
Indeed, when we have more than one hundred tests, summaries are less enjoyable
to read.
This module allows you to use html rendering Behat to have easy access to the
results.

LICENSES
==========
This module use simplehtmldom PHP library and SfYaml which are released
under the MIT license - compatible with GPL 2.0.



REQUIREMENTS
============

You'll need to download two third party code:

* simplehtmldom : http://sourceforge.net/projects/simplehtmldom/files/
You have to put the simple_html_dom.php file as it follows :
/sites/all/libraries/simplehtmldom/simple_html_dom.php

* sfYaml : http://sourceforge.net/projects/simplehtmldom/files/
You have to unpack and put it as it follows:
 /sites/all/libraries/sfYaml/lib/sfYaml.php
&& /sites/all/libraries/sfYaml/lib/sfYamlDumper.php
&& /sites/all/libraries/sfYaml/lib/sfYamlParser.php

It is assumed that you have a project Behat installed and configured as defined
 in the doc Behat, namely a tree as follows:

behat_directory:
--behat.yml
--features:
----all_my_features.feature
----bootstrap:
--------FeatureContext.php

look at http://docs.behat.org/cookbook/behat_and_mink.htmlgit stat

INSTALLATION:
=============

- Copy behat_integration directory to your modules directory
- Enable the module at: admin/build/modules
- Fill the config service at: admin/config/services/behat
- Create a new project at : admin/structure/behat/projects
- Launch you features Behat: admin/behat/launch